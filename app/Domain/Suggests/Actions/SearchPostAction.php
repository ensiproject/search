<?php

namespace App\Domain\Suggests\Actions;

class SearchPostAction
{
    public function __construct(protected ElasticSearchAction $action)
    {
    }

    public function execute(array $fields): array
    {
        $params = $this->createParams($this->prepareTextField($fields['text']));
        $hits = $this->action->execute($params);

        foreach ($hits as $hit) {
            $posts[] = $hit['_source'];
        }

        return $posts ?? [];
    }

    private function prepareTextField(string $text)
    {
        return htmlspecialchars(trim($text));
    }

    private function createParams(string $queryString)
    {
        return [
            'index' => 'posts',
            'body' => [
                'from' => 0,
                'size' => 10,
                'query' => [
                    'multi_match' => [
                        'query' => $queryString,
                        'type' => 'best_fields',
                        'fields' => [
                            'title_text^4',
                            'body_text',
                        ],
                        'tie_breaker' => 0.3,
                    ],
                ],
            ],
        ];
    }
}
