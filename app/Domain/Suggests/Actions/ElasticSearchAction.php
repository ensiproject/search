<?php

namespace App\Domain\Suggests\Actions;

use Elasticsearch\Client;

class ElasticSearchAction
{
    private Client $elasticsearch;

    public function __construct(Client $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch;
    }

    public function execute(array $params)
    {
        $response = $this->elasticsearch->search($params);

        return $response['hits']['hits'];
    }
}
