<?php

namespace App\Http\ApiV1\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class PostResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this['id'],
            'title' => $this['title_text'],
            'body' => $this['body_text'],
            'user_id' => $this['user_id'],
            'rating' => $this['rating'] ?? 0,

            'created_at' => $this['created_at'],
            'updated_at' => $this['updated_at'],

            'tags' => TagsResource::collection($this['tags'] ?? []),
            'hubs' => HubsResource::collection($this['hubs'] ?? []),
        ];
    }
}
