<?php

namespace App\Http\ApiV1\Controllers;

use App\Domain\Suggests\Actions\SearchPostAction;
use App\Http\ApiV1\Requests\SearchPostsRequest;
use App\Http\ApiV1\Resources\PostResource;

class SuggestsController
{
    public function search(SearchPostsRequest $request, SearchPostAction $action)
    {
        return PostResource::collectionWithoutPagination($action->execute($request->validated()));
    }
}
